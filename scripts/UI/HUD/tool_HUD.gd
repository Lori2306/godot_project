extends GridContainer

class_name ToolHUD

export(NodePath) onready var hud = get_node(hud) as HUD


func _ready():
	if GameEvents.connect("inventory_changed", self, "_on_inventory_changed") != OK:
		print("failure")


#Quando viene aggiunto un nuovo elemento all'inventario, controlla se si tratta di un Tool.
#Se il pannello del Tool è già presente, aggiorna i dati su di esso, altrimenti aggiunge un nuovo pannello
func _on_inventory_changed(_character, _inventory: Inventory, _item_changed: Dictionary):
	if _character == hud.get_character():
		#Recupero l'oggetto che è stato aggiunto all'inventario
		var _item: Item = _item_changed.item_reference
		
		if _item.tipology != Enums.ItemTipology.TOOL:
			return
		
		#Controllo se l'oggetto aggiunto è un Tool ed è stato sbloccato
		if _item_changed.status != Enums.ItemStatus.LOCKED:
			var _tool_item_UI_list: Array = self.get_children()
		
			var found: bool = false
			var found_item_UI: ToolItemUI = null
			#Controllo che il pannello che mostra il Tool non sia già stato aggiunto
			for _tool_item_UI in _tool_item_UI_list:
				_tool_item_UI = _tool_item_UI as ToolItemUI
				if _tool_item_UI.local_item.name != _item.name:
					continue
				
				found = true
				found_item_UI = _tool_item_UI
			
			if not found:
				add_new_panel(_character, _item)
			else:
				found_item_UI.update_quantity()


func add_new_panel(_character, _item: Item):
	var _new_tool_item_UI = load(_item.item_UI_path).instance() as ToolItemUI
	
	_new_tool_item_UI.setup(_item, _character)
	
	self.add_child(_new_tool_item_UI)
