extends HUD

class_name EnemyHUD

export(NodePath) onready var bot_name_label = get_node(bot_name_label) 


func _on_controller_changed(new_character: Character, _old_character: Character):
	._on_controller_changed(new_character, _old_character)
	
	if new_character == character:
		bot_name_label.text = character.get_statistics().display_name


func _on_current_life_changed(_life: Life, _character: Character, _has_increased: bool):
	if character == _character:
		if _has_increased:
			$AnimationPlayer.play("UI_recover_enemy")
		else:
			$AnimationPlayer.play("UI_hit_enemy")
