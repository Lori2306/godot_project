extends ItemUI

class_name ToolItemUI


func setup(_item: Item, _character: Character):
	.setup(_item, _character)
	
	var name_text: String = _item.display_name + ": "
	$HBoxContainer/NameLabel.text = name_text
	$HBoxContainer/TextureRect.texture = _item.avatar
	
	update_quantity()


func update_quantity():
	#Aggiorna la quantità in base a quella presente nell'inventario
	var inventory: Inventory = character.get_inventory()
	var quantity: int = inventory.get_item_quantity(local_item)
	
	$HBoxContainer/QuantityLabel.text = String(quantity)
	
	
	
