extends Manager

class_name SoundManager

export(NodePath) onready var audio_stream_player = get_node(audio_stream_player) as AudioStreamPlayer3D


func play_on_character_stream_player(sound_name: String, loop: bool = false, cut: bool = false) -> void:
	var sound_list: Dictionary = character.get_statistics().sound_list
	
	Util.play_random_sound_from_name(sound_name, sound_list, audio_stream_player, loop, cut)


func play_stream_on_default(stream: AudioStream, loop: bool = false, cut: bool = false ):
	Util.play_sound(audio_stream_player, stream, loop, cut)
