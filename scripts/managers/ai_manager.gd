extends Manager
class_name AIManager

#Export variables
export(NodePath) onready var _agent = get_node(_agent) as NavigationAgent

export(float) var player_offset
export(float) var enemy_offset
export(float) var view_offset

#Bersaglio salvato
onready var target: Node = null

#Ultima posizione salvata del bersaglio
onready var last_seen_position: Vector3

#Timers
onready var shoot_timer: Timer		#Timer che scandisce i momenti in cui si spara (AUTOSTART = false; ONE_SHOT = true)
onready var lost_target_timer: Timer		#Questo timer viene avviato quando il bersaglio non è più nel campo visivo; 
											#quando questo timer scade, viene resettato il bersaglio (AUTOSTART = false; ONE_SHOT = true)
onready var idle_path_timer: Timer			#Timer per aggiornare i punti dell'idle path (AUTOSTART = false; ONE_SHOT = true)
onready var update_target_location_timer: Timer		#Timer per aggiornare la posizione del target (AUTOSTART = false; ONE_SHOT = true)
onready var watchdog_timer: Timer		#Timer per evitare che rimanga bloccato da qualche parte per troppo tempo

#Informazione del personaggio
onready var runtime_data = character.get_runtime_data() as RuntimeData
onready var can_shoot: bool = true
onready var spawn_position: Vector3

#Indice dei punti dell'idle path
var idle_path_index: int = 0

#Offset
var offset: Vector3
var view_offset_vector: Vector3


#########################################################
########### 		INIZIALIZZAZIONE			#########
#########################################################

func _ready():
	setup_all_timers() 
	
	offset = Vector3(0, player_offset, 0)
	view_offset_vector = Vector3(0, view_offset, 0)
	
	spawn_position = character.global_transform.origin
	
	if GameEvents.connect("target_changed", self, "_on_target_changed") != OK:
		print("failure")
	if GameEvents.connect("character_shot", self, "_on_character_shot") != OK:
		print("failure")
	if GameEvents.connect("change_controller", self, "_on_controller_changed") != OK:
		print("failure")
	if GameEvents.connect("piece_ripped", self, "_on_piece_ripped") != OK:
		print("failure")
	if GameEvents.connect("died", self, "_on_died") != OK:
		print("failure")
	if GameEvents.connect("hit", self, "_on_hit") != OK:
		print("failure")
	
	#Connetto il timeout dei timer alle callback
	if shoot_timer.connect("timeout", self, "_on_shoot_timer_timeout") != OK:
		print("failure")
	if lost_target_timer.connect("timeout", self, "_on_lost_target_timer_timeout") != OK:
		print("failure")
	if idle_path_timer.connect("timeout", self, "_on_idle_path_timer_timeout") != OK:
		print("failure")
	if update_target_location_timer.connect("timeout", self, "_on_update_target_location_timer_timeout") != OK:
		print("failure")
	if watchdog_timer.connect("timeout", self, "_on_watchdog_timer_timeout") != OK:
		print("failure")
	
	character.set_current_ai_state(Enums.AIState.START)
	
	call_deferred("set_view_distance")


#Imposta i timer
func setup_all_timers():
	var lost_target_time: float = character.get_statistics().lost_target_time
	var shoot_time: float = character.get_statistics().shoot_time
	var idle_path_time: float = character.get_statistics().idle_path_time
	var update_target_location_time: float = character.get_statistics().update_target_location_time
	var watchdog_time: float = character.get_statistics().watchdog_time
	
	lost_target_timer = Util.setup_timer(lost_target_timer, self, lost_target_time, false, true)
	shoot_timer = Util.setup_timer(shoot_timer, self, shoot_time, false, true)
	idle_path_timer = Util.setup_timer(idle_path_timer, self, idle_path_time, true, true)
	update_target_location_timer = Util.setup_timer(update_target_location_timer, self, update_target_location_time, false, false)
	watchdog_timer = Util.setup_timer(watchdog_timer, self, watchdog_time, false, true)


func set_view_distance():
	var max_view_distance: int = character.get_statistics().max_view_distance
	var max_alert_distance: int = character.get_statistics().max_alert_distance

	character.get_view_area().get_child(0).shape.radius = max_alert_distance
	character.get_line_of_sight_raycast().cast_to = Vector3(0, 0, -max_view_distance)
	
	if character.get_view_area().connect("body_entered", self, "_on_view_area_body_entered") != OK:
		print("failure")


#########################################################
########### 		TASK PRINCIPALE				#########
#########################################################

func ai_movement(delta):
	if character.get_is_alive():
		if target and is_instance_valid(target):
			character.get_line_of_sight_raycast().look_at(view_offset_vector + target.global_transform.origin, Vector3.UP)
		
		match character.get_current_ai_state():
			Enums.AIState.START:
				start(delta)
				
				#print("Start")	#Debug
			Enums.AIState.IDLE:
				idle(delta)
				
				#print("Idle")	#Debug
			Enums.AIState.TARGET_AQUIRED:
				target_aquired(delta)
				
				#print("Target aquired")	#Debug
			Enums.AIState.ENGAGE_FIGHT:
				engage_fight(delta)
				
				#print("Engage fight")	#Debug
			Enums.AIState.ATTACKING:
				attacking(delta)
				
				#print("Attacking")	#Debug
			Enums.AIState.APPROACHING:
				approaching(delta)
				
				#print("Approaching")	#Debug
			Enums.AIState.AIMING:
				aiming(delta)
				
				#print("Aiming")	#Debug
			Enums.AIState.SHOOTING:
				shooting(delta)
				
				#print("Shooting")	#Debug
			Enums.AIState.DODGING:
				dodging(delta)
				
				#print("Shooting")	#Debug
			Enums.AIState.LOST_TARGET:
				lost_target(delta)
				
				#print("Lost target")
			Enums.AIState.SEARCHING:
				searching(delta)
				
				#print("Searching")	#Debug


#########################################################
########### 	FUNZIONI DI BASE DELL'AI		#########
#########################################################
#Possono essere reimplementate in classi specifiche

#Chiamata per inizializzare il bot
#	START -> 
#	1. IDLE: senza condizione
func start(_delta):
	#Inizializza lo stato idle
	
	#Resetta idle_path_index
	idle_path_index = 0
	
	#Imposta il primo punto da raggiungere
	var next_idle_point: Vector3 = get_next_idle_point()
	_agent.set_target_location(next_idle_point)
	
	#Cambia stato
	character.set_current_ai_state(Enums.AIState.IDLE)


#Chiamata quando il bot non ha un bersaglio ed è in ricognizione
#	IDLE -> 
#	1. ENGAGE_FIGHT: se viene acquisito un bersaglio
func idle(_delta):
	#Ricognizione
	calculate_velocity(_delta)
	
	look_at_path(_delta)
	
	#Decidi se cambiare stato
	if target:
		character.set_current_ai_state(Enums.AIState.TARGET_AQUIRED)


#Bersaglio aquisito
#TARGET_AQUIRED -> 
#	1. ENGAGE_FIGHT: se il bersaglio è nel campo visivo
#	2. LOST_TARGET: se il bersaglio non è nel campo visivo
func target_aquired(_delta):
	#Decidi se cambiare stato
	if is_target_in_direct_sight():
		character.set_current_ai_state(Enums.AIState.ENGAGE_FIGHT)
	else:
		character.set_current_ai_state(Enums.AIState.LOST_TARGET)


#Punto di partenza di un attacco
#ENGAGE_FIGHT -> 
#	1. ATTACKING: se il bersaglio è a debita distanza
#	2. DODGING: se il bersaglio è troppo vicino
func engage_fight(_delta):
	#Decidi se cambiare stato
	if is_target_too_close():
		character.set_current_ai_state(Enums.AIState.DODGING)
	else:
		character.set_current_ai_state(Enums.AIState.ATTACKING)


#Chiamata per iniziare a sferrare un attacco
#	ATTACKING -> 
#	1. APPROACHING: se il bersaglio è nel campo visivo
#	2. LOST_TARGET: se il target esce dal campo visivo
func attacking(_delta):
	#Decidi se cambiare stato
	if is_target_in_direct_sight():
		character.set_current_ai_state(Enums.AIState.APPROACHING)
	else:
		character.set_current_ai_state(Enums.AIState.LOST_TARGET)


#Chiamata per far avvicinare il bot al target
#	APPROACHING -> 
#	1. AIMING: se il bersaglio è nel campo visivo ed entro la portata dell'arma
#	2. LOST_TARGET: se il target esce dal campo visivo
func approaching(_delta):
	#Fai avvicinare al target. 
	
	calculate_velocity(_delta)
	
	aim(_delta)
	
	#Decidi se cambiare stato
	if is_target_in_direct_sight():
		if  is_target_in_shoot_range() and is_weapon_sight_free():
			update_target_location_timer.stop()
			character.set_current_ai_state(Enums.AIState.AIMING)
		else:
			if update_target_location_timer.is_stopped():
				update_target_location_timer.start()
	else:
		update_target_location_timer.stop()
		character.set_current_ai_state(Enums.AIState.LOST_TARGET)


#Chiamata per orientare il bot nella direzione del target.
#	AIMING -> 
#	1. SHOOTING: se il bersaglio è nel campo visivo, entro la portata dell'arma e sotto tiro
#	2. APPROACHING: se il bersaglio è nel campo visivo, ma fuori dalla portata dell'arma
#	3. LOST_TARGET: se il target esce dal campo visivo
func aiming(_delta):
	#Aim
	aim(_delta)
	
	calculate_velocity(_delta, true)
	
	#Decidi se cambiare stato
	if is_target_in_direct_sight():
		if  is_target_in_shoot_range() and is_weapon_sight_free():
			if is_target_aquired():
				character.set_current_ai_state(Enums.AIState.SHOOTING)
			else:
				return
		else:
			character.set_current_ai_state(Enums.AIState.APPROACHING)
	else:
		character.set_current_ai_state(Enums.AIState.LOST_TARGET)


#Chiamata quando il target è sotto tiro
# 	SHOOTING ->
#	1. DODGING: una volta terminato l'attacco oppure se viene colpito
func shooting(_delta):
	calculate_velocity(_delta, true)
	
	aim(_delta)
	
	#Fai partire lo shooting timer
	if shoot_timer.is_stopped():
		shoot_timer.start()


#Chiamata al termine di un attacco. Il bot si sposta in un punto a caso vicino al target
# 	DODGING ->
#	1. TARGET_AQUIRED: una volta terminata la schivata
#	2. LOST_TARGET: se il target esce dal campo visivo
func dodging(_delta):
	calculate_velocity(_delta, false)
	
	aim(_delta)
	
	if not is_target_in_direct_sight():
		character.set_current_ai_state(Enums.AIState.LOST_TARGET)


#Chiamata quando il target esce dal campo visivo
# 	LOST_TARGET ->
#	1. SEARCHING: senza condizione
func lost_target(_delta):
	#Reset
	
	#Start lost target timer
	lost_target_timer.start()
	
	#Salva l'ultima posizione del bersaglio
	update_last_seen_position()
	
	_agent.set_target_location(last_seen_position)
	
	#Cambia stato: comincia a cercare
	character.set_current_ai_state(Enums.AIState.SEARCHING)


#Chiamata quando il bot perde il contatto visivo del target.
#Serve a raggiungere l'ultima posizione nota.
# 	SEARCHING ->
#	1. ENGAGE_FIGHT: il target entra nel campo visivo
#	2. START: scade il timer
func searching(_delta):
	#Cerca il target nell'ultima posizione nota
	
	calculate_velocity(_delta)
	
	look_at_path(_delta)
	
	#Decidi se cambiare stato
	if is_target_in_direct_sight():
		lost_target_timer.stop()
		character.set_current_ai_state(Enums.AIState.ENGAGE_FIGHT)


#########################################################
########### 		NAVIGATION FUNCTIONS		#########
#########################################################
#Invocata da NavigationAgent dopo che viene computata la velocità
func move(_safe_velocity) -> void:
	character.set_instant_velocity(_safe_velocity)


func _on_NavigationAgent_navigation_finished():
	if character.get_current_ai_state() == Enums.AIState.IDLE:
		idle_path_timer.start()
	elif character.get_current_ai_state() == Enums.AIState.DODGING:
		#Dopo aver eseguito la schivata, torna nello stato di riposo
		watchdog_timer.stop()
		character.set_current_ai_state(Enums.AIState.TARGET_AQUIRED)



#Calcola e imposta la velocità verso il punto successivo da raggiungere
#	NOTA: da invocare dopo aver settato la destinazione nel NavigationAgent, all'interno di quelle
#	funzioni che prevedono che il bot si muova
func calculate_velocity(delta, stop: bool = false) -> void:
	if character.get_is_able_to_move():
		#Ottengo la velocità dal personaggio
		var _velocity: Vector3 = character.get_velocity()
		
		#Controlla se il punto da raggiungere non è stato raggiunto
		if _agent.is_navigation_finished() or stop:
			#Nel caso fosse raggiunto, procedo a fermare il robot
			_velocity = Vector3.ZERO
		else:
			#Direzione verso il prossimo punto da raggiungere
			var character_location = character.get_global_transform().origin
			var new_direction: Vector3 = character_location.direction_to(_agent.get_next_location())
			
			# Floor normal.
			var floor_raycast: RayCast = character.get_floor_raycast()
			var floor_normal: Vector3 = floor_raycast.get_collision_normal()
			if floor_normal.length_squared() < 0.001:
				# Set normal to Y+ if on air.
				floor_normal = Vector3(0, 1, 0)
			
			new_direction = new_direction.slide(floor_normal)
			
			# Calcola la  velocità
			var move_speed: float = character.get_statistics().move_speed
			var desired_velocity: Vector3 = new_direction * move_speed
			var steerinig: Vector3 = (desired_velocity - _velocity) * delta * 4.0
			
			_velocity += steerinig
		
		#Salvo la velocità, ma non sposto il personaggio. 
		#Verrà poi impostata una volta che il NavigationAgent avrà computato quella corretta
		character.set_instant_velocity(_velocity, false)
		
		#Imposta la velocità nell'agent
		_agent.set_velocity(_velocity)

#########################################################
########### 		UTILITIES					#########
#########################################################

#Aggiorna l'ultima posizione del bersaglio, prima di essere scomparso dal campo visivo
func update_last_seen_position():
	if target:
		last_seen_position = target.global_transform.origin


func get_random_location_from(initial_location: Vector3, max_distance: float, min_distance: float) -> Vector3:
	Util.rng.randomize()
	var _offset = Vector3(Util.rng.randf_range(-max_distance, max_distance), 0 , Util.rng.randf_range(-max_distance, max_distance))
	#Controlla che la distanza dal target sia superiore a quella minima
	if _offset.length() < min_distance:
		_offset = _offset.normalized() * min_distance
	
	var random_location = _offset + initial_location
	random_location.y = initial_location.y
	
	return random_location


func get_dodge_location_from(initial_location: Vector3, max_distance: float, min_distance: float) -> Vector3:
	Util.rng.randomize()
	var _offset: float = Util.rng.randf_range(-max_distance, max_distance)
	
	if abs(_offset) < min_distance:
		var positiv: float = _offset/abs(_offset)
		_offset = positiv * min_distance
	
	var target_location: Vector3 = target.global_transform.origin
	var character_location: Vector3 = character.global_transform.origin
	var character_to_target_direction: Vector3 = character_location.direction_to(target_location)
	
	var perpendicular_direction = character_to_target_direction.rotated(Vector3.UP, PI/2)
	
	var dodge_location: Vector3 = initial_location + perpendicular_direction * _offset 
	
	return dodge_location


#Incrementa idle_path_index e recupera il successivo punto da raggiungere
func get_next_idle_point() -> Vector3:
	#Recupera tutti i punti
	var idle_points: Array = character.get_idle_points()
	var next_idle_point: Vector3
	
	#Mantieni l'indice tra 0 e (idle_points.size - 1)
	idle_path_index %=  idle_points.size()
	
	#Ottieni il prossimo punto da raggiungere
	next_idle_point = idle_points[idle_path_index]
	
	#Incrementa l'indice
	idle_path_index += 1
	
	return next_idle_point


#########################################################
########### 		CALLBACK DEI TIMER			#########
#########################################################

func _on_lost_target_timer_timeout():
	character.set_current_ai_state(Enums.AIState.START)
	GameEvents.emit_signal("target_changed", null, character)


#Allo scadere del timer, termina la sequenza di attacco e si può tornare allo stato di bersaglio acquisito
func _on_shoot_timer_timeout():
	if character.get_current_ai_state() == Enums.AIState.SHOOTING:
		#Torna nello stato di riposo, quando è presente un bersaglio
		start_dodging()
	else:
		#Se per qualche motivo il timer termina in uno stato diverso da SHOOTING, resetta
		character.set_current_ai_state(Enums.AIState.START)


func start_dodging():
	if target:
		#Calcola un punto vicino al target e imposta destinazione
		var target_location: Vector3 = target.global_transform.origin
		var max_dodge_radius: float = character.get_statistics().max_dodge_radius
		var min_dodge_radius: float = character.get_statistics().min_dodge_radius
		var random_point_near_target: Vector3 = get_dodge_location_from(target_location, max_dodge_radius, min_dodge_radius)
		
		_agent.set_target_location(random_point_near_target)
		
		#Fai partire il watchdog timer: se scade questo timer, significa che il target è bloccato
		watchdog_timer.start()
		
		character.set_current_ai_state(Enums.AIState.DODGING)

#Allo scadere dell'idle path timer, aggiorna la posizione da raggiungere del NavigationAgent
func _on_idle_path_timer_timeout():
	if character.get_current_ai_state() == Enums.AIState.IDLE:
		var next_idle_point: Vector3 = get_next_idle_point()
		
		_agent.set_target_location(next_idle_point)


#Allo scadere del timer viene aggiornata la posizione da raggiungere
func _on_update_target_location_timer_timeout():
	if character.get_current_ai_state() == Enums.AIState.APPROACHING:
		var target_location: Vector3 = target.global_transform.origin
		
		_agent.set_target_location(target_location)


func _on_watchdog_timer_timeout():
	character.set_current_ai_state(Enums.AIState.START)

#####################################################################
#### FUNZIONI PER CONTROLLARE LO STATO DEL BERSAGLIO E DEL ROBOT ####
#####################################################################

#Controlla se il bersaglio è nel campo visivo del robot
func is_target_in_direct_sight() -> bool:
	var collider = character.get_line_of_sight_raycast().get_collider()
	
	if collider:
		if collider == target:
			return true
	
	return false


#Controlla se l'arma del robot non è coperta da un muro
func is_weapon_sight_free() -> bool:
	var weapon_collider = character.get_weapon_line_of_sight_raycast().get_collider()
	
	if weapon_collider:
		if weapon_collider == target\
		or weapon_collider is Bullet:
			return true
	else:
		return true
	
	return not character.get_is_able_to_aim()


#Controlla se il bersaglio è nella portata dell'arma
func is_target_in_shoot_range() -> bool:
	var distance: float = (target.translation - character.translation).length()
	var weapon_range: float = character.get_current_weapon().max_distance
	
	return distance <= weapon_range


#Controlla se il bersaglio è sotto tiro
func is_target_aquired() -> bool:
	var shooting_raycast: RayCast = character.get_shooting_raycast()
	var collider = shooting_raycast.get_collider()
	
	if collider:
		if collider is Shootable:
			return collider.character == target
		else:
			return false
	else:
		return false


#TODO: implementare: controlla se il bersaglio è troppo vicino
func is_target_too_close() -> bool:
	return false


#########################################################
#### 			FUNZIONI DI MOVIMENTO				 ####
#########################################################

func look_at_path(delta) -> void:
	var path = _agent.get_nav_path()
	var index: int = _agent.get_nav_path_index()

	if path.size() > 0:
		var look_at_point: Vector3 = path[index]
		look_at_point.y = character.global_transform.origin.y
		var turning_speed: float = character.get_statistics().turning_speed
		
		character.set_as_toplevel(true)
		character.transform = character.smooth_look_at(character, look_at_point, turning_speed, delta)
		character.set_as_toplevel(false)


func look_at_target(delta):
	var upper_part: Spatial = character.get_upper_part()
	var turning_speed: float = character.get_statistics().turning_speed

	upper_part.set_as_toplevel(true)
	upper_part.transform = character.smooth_look_at(upper_part, Vector3(target.translation.x, upper_part.transform.origin.y, target.translation.z), turning_speed, delta)
	upper_part.set_as_toplevel(false)


func aim(delta) -> void:
	var turning_speed: float = character.get_statistics().turning_speed
	var upper_part: Spatial = character.get_upper_part()
	
	upper_part.set_as_toplevel(true)
	upper_part.transform = character.smooth_look_at(upper_part, offset + target.global_transform.origin, turning_speed, delta)
	upper_part.set_as_toplevel(false)


#########################################################
#### 			CALLBACK DEI SEGNALI				 ####
#########################################################

#Invocata quando un personaggio spara
func _on_character_shot(_character):
	if character.get_is_active():
		if _character.is_in_group("trigger") or _character.is_in_group("resistance"):
			if not target:
				var distance: float = (self.character.translation - _character.translation).length()
				var max_hear_distance: float = self.character.get_statistics().max_hear_distance
				
				if distance < max_hear_distance:
					GameEvents.emit_signal("target_changed", _character, self.character)
					
					if _character.is_in_group("trigger"):
						_character.add_to_group("resistance")
						_character.remove_from_group("trigger")
			else:
				update_last_seen_position()


#Invocata quando si cambia bersaglio
func _on_target_changed(_target, _character):
	var new_target = null
		
	if _character == character:
		if _target:
			if _target.get_is_alive():
				new_target = _target
		
		target = new_target
		
		update_last_seen_position()
		_agent.set_target_location(last_seen_position)


#Invocata quando si cambia personaggio
func _on_controller_changed(new_controller, old_controller) -> void:
	if old_controller:
		if old_controller == target:
			GameEvents.emit_signal("target_changed", new_controller, character)
		
	if new_controller.is_in_group("player"):
		offset = Vector3(0, player_offset, 0)
	elif new_controller.is_in_group("enemy"):
		offset = Vector3(0, enemy_offset, 0)


#Invocata quando viene distrutto un pezzo del robot
func _on_piece_ripped(_character, piece: DismountablePiece):
	if _character == character:
		var piece_tipology: int = piece.get_piece_tipology()
		match piece_tipology:
			Enums.PieceTipology.HEAD:
				character.set_is_able_to_aim(false)
			Enums.PieceTipology.WEAPON:
				character.set_is_able_to_shoot(false)
			Enums.PieceTipology.LEG:
				character.set_is_able_to_move(false)


#Invocata quando un personaggio entra nell'area visiva del robot
func _on_view_area_body_entered(body):
	if character.get_is_active():
		if not target:
			if body.is_in_group("resistance"):
				GameEvents.emit_signal("target_changed", body, character)


func _on_hit(_area_hit: Shootable, damage: int):
	if _area_hit.get_character() == self.character:
		#start_dodging()
		pass


#Invocata alla morte del robot
func _on_died(_character):
	if _character == target:
		if target.is_in_group("player"):
			GameEvents.emit_signal("target_changed", null, self.character)
