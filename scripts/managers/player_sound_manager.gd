extends SoundManager

class_name PlayerSoundManager

export(NodePath) onready var pickup_audio_stream_player = get_node(pickup_audio_stream_player) as AudioStreamPlayer3D


func play_stream_on_pickup(stream: AudioStream, loop: bool = false, cut: bool = false ):
	Util.play_sound(pickup_audio_stream_player, stream, loop, cut)
