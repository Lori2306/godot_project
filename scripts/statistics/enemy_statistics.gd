extends ActiveCharacterStatistics

class_name EnemyStatistics


export(float) var max_view_distance:= 60.0
export(float) var max_alert_distance:= 10.0

#Time for timers
export(float) var lost_target_time:= 20.0
export(float) var shoot_time:= 1.5
export(float) var idle_path_time:= 2.0
export(float) var update_target_location_time:= 0.1
export(float) var watchdog_time:= 10.0

export(float) var speak_time:= 5.0

export(float) var max_hear_distance:= 30.0

#Radius
export(float) var min_dodge_radius:= 5.5
export(float) var max_dodge_radius:= 7.7

#Array di due dimensioni: la prima dimensione sono le tipologie di suono che il character deve emettere quando passa in uno degli stati
#elencati in Enums.AIState. La seconda dimensione contiene delle varianti del suono stesso(Es: laugh1, laugh2, ...)
export(Dictionary) var ai_sound_list = {
	"start" : [],
	"idle" : [preload("res://assets/my_assets/sounds/carl_dialogues/idle_1.wav"),
		preload("res://assets/my_assets/sounds/carl_dialogues/idle_2.wav")],
	"aiming" : [preload("res://assets/my_assets/sounds/carl_dialogues/aiming_1.wav"),
		preload("res://assets/my_assets/sounds/carl_dialogues/aiming_2.wav")],
	"target_acquired" : [preload("res://assets/my_assets/sounds/carl_dialogues/target_acquired_1.wav"),
		preload("res://assets/my_assets/sounds/carl_dialogues/target_acquired_2.wav"),
		preload("res://assets/my_assets/sounds/carl_dialogues/target_acquired_3.wav"),
		preload("res://assets/my_assets/sounds/carl_dialogues/target_acquired_4.wav"),],
	"dodging" : [],
	"hurt" : [],
	"approaching" : [preload("res://assets/my_assets/sounds/carl_dialogues/approaching_1.wav"), 
		preload("res://assets/my_assets/sounds/carl_dialogues/approaching_2.wav"),
		preload("res://assets/my_assets/sounds/carl_dialogues/approaching_3.wav")],
	"searching" : [preload("res://assets/my_assets/sounds/carl_dialogues/searching_1.wav"),
		preload("res://assets/my_assets/sounds/carl_dialogues/searching_2.wav"),
		preload("res://assets/my_assets/sounds/carl_dialogues/searching_3.wav")],
	}
