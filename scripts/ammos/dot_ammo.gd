extends Ammo

class_name DotAmmo

export(Texture) var bullet_hole_texture = preload("res://assets/my_assets/textures/bullet_hole_texture.png")
export(bool) var is_bullet_hole_glowing = false
export(bool) var is_bullet_hole_smoking = false
export(float) var bullet_hole_scale = 0.1
