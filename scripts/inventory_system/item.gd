extends Resource

class_name Item

export(String) var name
export(String) var display_name
export(Texture) var avatar
export(Resource) var description = preload("res://my_resources/slides/no_description.tres") as Slide
export(Mesh) var display_mesh
export(bool) var is_unique
export(int) var max_quantity
export(Enums.ItemTipology) var tipology
export(Color) var light_color = Color.lightblue
export(AudioStream) var pickup_sound = preload("res://assets/my_assets/sounds/pixabay_sound_effects/item-pickup-37089.mp3")
export(String) var item_UI_path = "res://nodes/UI/tool_item_UI.tscn"
export(bool) var show_message_when_picked_up


func get_avatar() -> Texture:
	return avatar


func get_mesh() -> Mesh:
	return display_mesh
