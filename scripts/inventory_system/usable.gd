extends Stockable

class_name Usable

export(AudioStream) var use_sound = preload("res://assets/my_assets/sounds/life_recover.wav")


func use(_character):
	_character.sound_manager.play_stream_on_default(use_sound, false, true)	#Loop= false, Cut = true
			
	print(self.name + " has been used by " + _character.name)
