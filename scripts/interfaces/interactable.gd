extends Area

class_name Interactable

#Flag
var is_used: bool setget set_is_used, get_is_used
var is_active: bool = true setget set_is_active, get_is_active
#Se impostato a true, dopo che l'oggetto viene utilizzato, si disabilita
export(bool) onready var one_shot setget set_one_shot, get_one_shot

#Testo da mostrare
export(String) onready var interaction_text

#Sound related variables
export(bool) onready var is_sound_on
export(NodePath) onready var audio_stream_player = get_node_or_null(audio_stream_player) as AudioStreamPlayer3D
export(NodePath) onready var interact_audio_stream_player = get_node_or_null(interact_audio_stream_player) as AudioStreamPlayer3D
export(AudioStream) var sound
export(AudioStream) var interact_sound


#########################################################
########### 		INIZIALIZZAZIONE				#########
#########################################################

func _ready():
	if GameEvents.connect("interact", self, "_on_interact") != OK:
		print("failure")
	
	if is_sound_on:
		play_sound()

#########################################################
########### 		API PRINCIPALE				#########
#########################################################

#Chiamata quando si utilizza per la prima volta
func use(_character: Character):
	if one_shot:
		set_is_used(true)


#########################################################
########### 		CALLBACK 					#########
#########################################################

func _on_interact(character: Character, interactable_object):
	if interactable_object == self and is_active:
		print(character.name + " has interacted with " + get_parent().name)
		
		play_interaction_sound()
		
		if not is_used:
			use(character)


#########################################################
########### 		UTILITIES					#########
#########################################################


func play_interaction_sound(on_loop: bool = false):
	if interact_audio_stream_player:
		interact_sound.loop = on_loop
		interact_audio_stream_player.stream = interact_sound
		
		interact_audio_stream_player.play()


func play_sound(on_loop: bool = true):
	if audio_stream_player:
		sound.loop = on_loop
		audio_stream_player.stream = sound
		
		audio_stream_player.play()


func set_is_used(_is_used: bool) -> void:
	is_used = _is_used


func get_is_used() -> bool:
	return is_used


func set_is_active(_is_active: bool) -> void:
	is_active = _is_active


func get_is_active() -> bool:
	return is_active


func set_one_shot(_one_shot: bool) -> void:
	one_shot = _one_shot


func get_one_shot() -> bool:
	return one_shot


func get_interaction_text() -> String:
	return interaction_text
