extends Interactable

class_name Switch

signal switch_pressed

export(NodePath) onready var animation_player = get_node(animation_player) as AnimationPlayer


#Override
#Chiamata quando si interagisce con l'oggetto la prima volta
func use(_character: Character):
	.use(_character)
	
	#Segnala che lo switch è stato premuto
	emit_signal("switch_pressed", self)
	animation_player.play("push")
