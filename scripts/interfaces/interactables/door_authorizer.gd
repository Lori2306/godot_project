extends Interactable

class_name DoorAuthorizer

export(NodePath) onready var door = get_node(door) as Door
export(NodePath) onready var mesh_reference = get_node(mesh_reference) as MeshInstance

#Sounds
export(NodePath) onready var authorization_audio_stream_player = get_node_or_null(authorization_audio_stream_player) as AudioStreamPlayer3D
export(AudioStream) var not_authorized_sound
export(AudioStream) var authorized_sound

#Oggetto nell'inventario che permette di sbloccare la porta
export(Resource) var unlock_item = unlock_item as Item
export(Resource) var unlock_item_material


func _ready():
	update_display_mesh()

#########################################################
########### 		API PRINCIPALE				#########
#########################################################

#Override
func use(_character: Character):
	.use(_character)
	
	var inventory: Inventory = _character.get_inventory()
	var is_authorized: bool = inventory.is_item_in_stock(unlock_item)
	
	play_authorizazion_sound(is_authorized)
	
	if is_authorized:
		#Sblocca la porta se la chiave è nell'inventario
		GameEvents.emit_signal("lock_door", door, false)
		
		#Togli la chiave dall'inventario
		var quantity: int = inventory.get_item_quantity(unlock_item)
		var new_quantity = max(quantity-1, 0)
		inventory.set_item_quantity(_character, unlock_item, new_quantity)
		
		mesh_reference.material_override = unlock_item_material


#########################################################
########### 		UTILITIES				#########
#########################################################

func play_authorizazion_sound(authorize: bool = false):
	if authorization_audio_stream_player:
		var stream: AudioStream = not_authorized_sound
		
		if authorize:
			stream = authorized_sound
		
		authorization_audio_stream_player.stream = stream
		
		authorization_audio_stream_player.play()


func update_display_mesh():
	mesh_reference.mesh = unlock_item.display_mesh
