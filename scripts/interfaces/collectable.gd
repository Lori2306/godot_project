tool
extends RigidBody

class_name Collectable

export(Resource) var item = item as Item
export(int) var quantity

export(float) onready var move_speed
export(float) onready var max_attraction_distance

onready var _set: bool = false
onready var _target = null


func _ready():
	if GameEvents.connect("collected", self, "_on_collected") != OK:
		print("failure")


func _process(_delta):
	if not _set:
		if item:
			get_node("MeshInstance").mesh = item.display_mesh
			get_node("OmniLight").light_color = item.light_color
			_set = true


func _physics_process(delta):
	#Se un personaggio è vicino, fai avvicinare l'oggetto ad esso
	if _target:
		var item_position: Vector3 = self.global_transform.origin
		var target_position: Vector3 = _target.global_transform.origin + 0.2*Vector3.UP
		
		var distance: float = item_position.distance_to(target_position)
		
		#Se il player è troppo distante, smetti di inseguire
		if distance > max_attraction_distance:
			_target = null
		else:
			var direction = item_position.direction_to(target_position) * move_speed
			self.apply_central_impulse(direction)
	
	#Spin
	get_node("MeshInstance").rotation_degrees.y += 180*delta


#Quando la area di collezione del player, entra in contatto con quest'area, l'oggetto viene attratto da essa
func _on_Area_area_entered(_area):
	if _area.get_parent().is_in_group("player"):
		var player = _area.get_parent()
		_target = player


func _on_collected(body: Collectable, _item: Item, _quantity: int, _character):
	if body == self:
		if _character.is_in_group("player"):
			GameEvents.emit_signal("add_item_to_inventory", _character, _item, _quantity)
		

		queue_free()


func set_item(_item: Item) -> void:
	item = _item

func get_item() -> Item:
	return item


func get_quantity() -> int:
	return quantity


