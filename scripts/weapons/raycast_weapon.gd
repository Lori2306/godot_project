extends DotWeapon

class_name RayCastWeapon


func shoot(character) -> void:
	.shoot(character)
	var shooting_raycast = character.get_shooting_raycast() as RayCast
	
	if shooting_raycast:
		var collider = character.get_shooting_raycast().get_collider()
	
		if collider:
			var spawn_location: Vector3 = shooting_raycast.get_collision_point() + 0.1 * shooting_raycast.get_collision_normal()
			self.spawn_bullet_hole(collider, shooting_raycast, spawn_location)
			
			if collider is Shootable:
				GameEvents.emit_signal("hit", collider, self.damage)

