extends Level

class_name Level1


export(NodePath) onready var switch = get_node(switch) as Switch
export(NodePath) onready var win_timer = get_node(win_timer) as Timer

export(Array, Array, NodePath) var enemy_list

var enemy_count: int
var stage_index: int = 0


func _ready():
	if GameEvents.connect("open_door", self, "_on_door_opened") != OK:
		print("failure")
	if GameEvents.connect("died", self, "_on_died") != OK:
		print("failure")
	if switch.connect("switch_pressed", self, "_on_switch_pressed") != OK:
		print("failure")
	
	if is_level_active:
		spawn_player()
	
		global_runtime_data.current_gameplay_state = Enums.GamePlayState.PLAY
		
		change_music("relax", true, true)
	else:
		global_runtime_data.current_gameplay_state = Enums.GamePlayState.TITLE
		
		change_music("title", true, true)


func _on_door_opened(_door: Door, _is_opened: bool):
	pass


func _on_died(character: Character):
	pass


func _on_switch_pressed(_switch: Switch):
	if _switch == switch:
		win_timer.start()


func _on_WinTimer_timeout():
	win()
