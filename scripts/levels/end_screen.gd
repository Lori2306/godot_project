extends Control


func _on_Title_Screen_pressed():
	GameEvents.emit_title_screen()


func _on_Skip_pressed():
	$AudioStreamPlayer.stop()
	$AnimationPlayer.play("show_all_text")
