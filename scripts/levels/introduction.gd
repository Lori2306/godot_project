extends Control

export(int) onready var number_of_text
export(String) onready var animation_text_prefix: String = "show_text_"

onready var current_text_number: int = 1
onready var current_animation: String = animation_text_prefix + String(current_text_number)

onready var animation_player = get_node("AnimationPlayer")


func _ready():
	animation_player.play(current_animation)

func _on_Continue_pressed():
	next_text()


func _on_Skip_pressed():
	$AudioStreamPlayer.stop()
	start_level()


func next_text():
	current_text_number += 1
	
	if current_text_number > number_of_text:
		start_level()
		
		return
	
	current_animation = animation_text_prefix + String(current_text_number)
	animation_player.play(current_animation)


func start_level():
	GameEvents.emit_signal("play", SceneManager.current_level_index + 1)
